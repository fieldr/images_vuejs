# Vue.js Image Viewer and Uploader using Imgur API

## How to install and use

- clone repository to your computer
- `cd` to the folder directory in your terminal
- run `yarn` to download and install dependencies
- run `yarn serve` to build 'development' version of the app. This will automatically open a browser tab, running on [localhost:8080](http://localhost:8080/) (unless already being used by another application)

## How to use

- to upload or view images, you will need an Imgur account
- press "Login" in the top right and log in using your Imgur credentials on the Imgur page
- go to "Galleries" to view images
- go to "Upload" to upload
- "Logout" to log out of Imgur