import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import store from './store/index';
import AuthHandler from './components/AuthHandler';
import ImageList from './components/ImageList';
import UploadForm from './components/UploadForm';

Vue.use(VueRouter);

export const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: ImageList},
    { path: '/upload', component: UploadForm},
    { path: '/oauth2/callback', component: AuthHandler }
  ]
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


// Imgur API OAuth Details
// Client ID: 97e35832df2117e
// Secret: 519ffdeb9de73144863ffd19506cafe433a324d0

// https://api.imgur.com/oauth2/authorize?
  // client_id=YOUR_CLIENT_ID&
  // response_type=REQUESTED_RESPONSE_TYPE&
  // state=APPLICATION_STATE

// http://localhost:8080/oauth2/callback#
  // access_token=84abae66683c5e251f54a6aa15df8caa76c57fee
  // &expires_in=315360000
  // &token_type=bearer
  // &refresh_token=967bb343d8b4f1eba2c47f3dce99efca2bc9798f
  // &account_username=fielddev92
  // &account_id=87542923